<!DOCTYPE html>
<html>
	<head>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<title>Register/Login</title>
		{{Html::style('css/bootstrap/bootstrap.css')}}
		{{Html::style('css/custom.css')}}
		{!! Html::style('css/datatables/dataTables.bootstrap.css') !!}
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	</head>
	<body class="login-body">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-lg-4 col-md-push-4 col-lg-push-4">
				<div class="auth-wrapper">
					<div class="auth-title">
						@yield('title')
					</div>
					<div class="auth-content">
						@yield('content')
					</div>
					<div class="auth-footer">
						@yield('footer')
					</div>
				</div>	
				</div>
			</div>
		</div>
	</body>

