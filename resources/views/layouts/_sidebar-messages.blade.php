<div class="sidenav">
	<ul>
		<li><a href="" >New</a></li>
		<li><a href="{{route('message.inbox')}}" >Inbox</a></li>
		<li><a href="{{route('message.sent')}}" >Sent</a></li>
		<li><a href="" >Trash</a></li>
		<li><a href="{{route('user.profile', ['id' => Auth::user() -> id])}}" >Back to Profile</a></li>
	</ul>
</div>