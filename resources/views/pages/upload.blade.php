@extends('layouts.master')

@section('title')
	Uplaod Images, Documents, Videos and Music files
@endsection

@section('content')

<div class="container">
	@if(count($errors) > 0)
		<div class="row">
			<div class="col-md-12 col-lg-12 col-sm-12">
				<ul class="alert alert-danger">
					@foreach($errors -> all() as $error)
						<li>{{$error}}</li>
					@endforeach
				</ul>
			</div>
		</div>
	@endif	
	@if(session('message'))
		<div class="row">
			<div class="col-md-12 col-lg-12 col-sm-12">
				{{session('message')}}
			</div>
		</div>
	@endif
	@if(session('error'))
		<div class="row">
			<div class="col-md-12 col-lg-12 col-sm-12 alert alert-danger">
				{{session('error')}}
			</div>
		</div>
	@endif
	@if(session('success'))
		<div class="row">
			<div class="col-md-12 col-lg-12 col-sm-12 alert alert-success">
				{{session('success')}}
			</div>
		</div>
	@endif
	@if(session('exist'))
	<?php $exist = session('exist'); ?>
		<div class="row">
			<div class="col-md-12 col-lg-12 col-sm-12 alert alert-danger">
				<ul>
					@foreach($exist as $ext)
						<li>{{$ext}}</li>
					@endforeach
				</ul>
			</div>
		</div>
	@endif
	<div class="row">
		<div class="col-md-3 col-lg-3 col-sm-2">
			<button type="button" id="buttImg" name="img" class="btn btn-primary" onClick="show1()">Image</button>
		</div>
		<div class="col-md-3 col-lg-3 col-sm-2">
			<button type="button" id="buttVideo" name="vid" class="btn btn-primary" onClick="show2()">Video</button>
		</div>
		<div class="col-md-3 col-lg-3 col-sm-2">
			<button type="button" id="buttFile" name="fil" class="btn btn-primary" onClick="show3()">Document</button>
		</div>
		<div class="col-md-3 col-lg-3 col-sm-2">
			<button type="button" id="buttMusic" name="msc" class="btn btn-primary" onClick="show4()">Music</button>
		</div>
	</div>
	<div class="row">
		<form name="form" action="/upload" method="post" enctype="multipart/form-data">
			<input type="hidden" name="_token" value="<?php echo csrf_token() ?>" >
			<div class="col-md-3 col-lg-3 col-sm-2 ">
				<div class="box" id="show1"  style="display: none">
					<div class="box-header with-border">
						 <h3 class="box-title">Uplaod Image <span id="hide1" onClick="hide1();">X</span></h3>
					</div>
					<div class="box-body">
					<div  id="bodyImg">
						<div class="form-group">
							<label>Image Name:</label>
							<input type="text" name="imgName" class="form-control" />
						</div>
						<div class="form-group">
							<label>Choose Image for Upload:</label> 
							<input type="file" name="imgFile" class="" />
						</div>
						<div class="form-group">
							<label>Description:</label> 
							<textarea name="imgDesc" class="form-control"></textarea>
						</div>
					</div>	
						<div class="form-group">
							<button type="button" class="btn btn-default" onClick="addDivImg(); addElementsImg()">Add another</button>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-md-3 col-lg-3 col-sm-2">
				<div class="box" id="show2"  style="display: none">
					<div class="box-header with-border">
						 <h3 class="box-title">Uplaod Video <span id="hide2" onClick="hide2();">X</span></h3>
					</div>
					<div class="box-body">
					<div id="bodyVideo">
						<div class="form-group">
							<label>Video Name:</label>
							<input type="text" name="vidName" class="form-control" />
						</div>
						<div class="form-group">
							<label>Choose Video for Upload:</label> 
							<input type="file" name="vidFile" class="form-control" />
						</div>
						<div class="form-group">
							<label>Description:</label> 
							<textarea name="vidDesc" class="form-control"></textarea>
						</div>
					</div>
						
						<div class="form-group">
							<button type="button" class="btn btn-default" onClick="addDivVideo(); addElementsVideo()">Add another</button>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-md-3 col-lg-3 col-sm-2">
				<div class="box" id="show3"  style="display: none">
					<div class="box-header with-border">
						 <h3 class="box-title">Upload Document <span id="hide3" onClick="hide3();">X</span></h3>
					</div>
					<div class="box-body">
					<div  id="bodyFile">
						<div class="form-group">
							<label>Document Name:</label>
							<input type="text" name="docName" class="form-control" />
						</div>
						<div class="form-group">
							<label>Choose Document for Upload:</label> 
							<input type="file" name="docFile" class="form-control" />
						</div>
						<div class="form-group">
							<label>Description:</label> 
							<textarea name="docDesc" class="form-control"></textarea>
						</div>
					</div>
						
						<div class="form-group">
							<button type="button" class="btn btn-default" onClick="addDivFile(); addElementsFile()">Add another</button>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-md-3 col-lg-3 col-sm-2">
				<div class="box" id="show4"  style="display: none">
					<div class="box-header with-border">
						 <h3 class="box-title">Upload Music<span id="hide4" onClick="hide4();">X</span></h3>
					</div>
					<div class="box-body">
					<div id="bodyMusic">
						<div class="form-group">
							<label>Song's Name:</label>
							<input type="text" name="mscName" class="form-control" />
						</div>
						<div class="form-group">
							<label>Choose song for Upload:</label> 
							<input type="file" name="mscFile" class="form-control" />
						</div>
						<div class="form-group">
							<label>Description:</label> 
							<textarea name="mscDesc" class="form-control"></textarea>
						</div>
					</div>
						
						<div class="form-group">
							<button type="button" class="btn btn-default" onClick="addDivMsc(); addElementsMsc()">Add another</button>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-lg-12">
					
				</div>
			</div>
				
					<div class="form-group">
						<input type="submit" value="Uplaod" class="btn btn-primary" />
					</div>
					
			</div>
		</form>
	</div>
</div>

<script>
function show1() {
	var spn = document.getElementById('show1');
    if(spn.style.display == "none") {
    	spn.style.display= "block";
    }
}
function hide1() {
	var spn = document.getElementById('show1');
	 if(spn.style.display == "block") {
    	spn.style.display= "none";
    }
}

function show2() {
	var spn = document.getElementById('show2');
    if(spn.style.display == "none") {
    	spn.style.display= "block";
    }
}
function hide2() {
	var spn = document.getElementById('show2');
	 if(spn.style.display == "block") {
    	spn.style.display= "none";
    }
}

function show3() {
	var spn = document.getElementById('show3');
    if(spn.style.display == "none") {
    	spn.style.display= "block";
    }
}
function hide3() {
	var spn = document.getElementById('show3');
	 if(spn.style.display == "block") {
    	spn.style.display= "none";
    }
}

function show4() {
	var spn = document.getElementById('show4');
    if(spn.style.display == "none") {
    	spn.style.display= "block";
    }
}
function hide4() {
	var spn = document.getElementById('show4');
	 if(spn.style.display == "block") {
    	spn.style.display= "none";
    }
}



function addDivImg() {
	var div = document.createElement('DIV');
    div.setAttribute('class', 'imgClass form-group');

	var tit = document.createTextNode('Add another Image')
    div.appendChild(tit);
    var dm = document.getElementById('bodyImg');
    dm.appendChild(div);
}
function addElementsImg() {
	var divs = document.getElementsByClassName('imgClass');
    var lngt = divs.length;
    for(var i = 0; i < lngt; i++) {
    	var lab = document.createElement('LABEL');
        var x = document.createTextNode('Image Name:');
        lab.appendChild(x);
    	var another = document.createElement('INPUT');
        another.setAttribute('type', 'text');
        another.setAttribute('name', 'imgName' + i);
        another.setAttribute('id', 'imgNameId' + i);
        another.setAttribute('class', 'form-control')
        
        var labFile = document.createElement('LABEL');
        var y = document.createTextNode('Chose Image for upload:');
        labFile.appendChild(y);
        var file = document.createElement('INPUT');
        file.setAttribute('type', 'file');
        file.setAttribute('name', 'imgFile' + i );
        file.setAttribute('id', 'imgFileId' + i);
        file.setAttribute('class', 'form-control')
        
        var labDesc = document.createElement('LABEL');
        var z = document.createTextNode('Description:');
        labDesc.appendChild(z);
        var desc = document.createElement('TEXTAREA');
        desc.setAttribute('name', 'imgDesc' + i );
        desc.setAttribute('id', 'imgDescId' + i);
        desc.setAttribute('class', 'form-control')
        
        if(!document.getElementById('imgNameId' + i)) {
        	divs[i].appendChild(lab); 
        	divs[i].appendChild(another); 
        }
         if(!document.getElementById('imgFileId' + i)) {
        	divs[i].appendChild(labFile); 
        	divs[i].appendChild(file); 
        }
         if(!document.getElementById('imgDescId' + i)) {
        	divs[i].appendChild(labDesc); 
        	divs[i].appendChild(desc); 
        }
    }
}


function addDivVideo() {
	var div = document.createElement('DIV');
    div.setAttribute('class', 'vidClass form-group');

	var tit = document.createTextNode('Add another Video')
    div.appendChild(tit);
    var dm = document.getElementById('bodyVideo');
    dm.appendChild(div);
}
function addElementsVideo() {
	var divs = document.getElementsByClassName('vidClass');
    var lngt = divs.length;
    for(var i = 0; i < lngt; i++) {
    	var lab = document.createElement('LABEL');
        var x = document.createTextNode('Video Name:');
        lab.appendChild(x);
    	var another = document.createElement('INPUT');
        another.setAttribute('type', 'text');
        another.setAttribute('name', 'vidName' + i);
        another.setAttribute('id', 'vidNameId' + i);
        another.setAttribute('class', 'form-control')
        
        var labFile = document.createElement('LABEL');
        var y = document.createTextNode('Chose Video for upload:');
        labFile.appendChild(y);
        var file = document.createElement('INPUT');
        file.setAttribute('type', 'file');
        file.setAttribute('name', 'vidFile' + i );
        file.setAttribute('id', 'vidFileId' + i);
        file.setAttribute('class', 'form-control')
        
        var labDesc = document.createElement('LABEL');
        var z = document.createTextNode('Description:');
        labDesc.appendChild(z);
        var desc = document.createElement('TEXTAREA');
        desc.setAttribute('name', 'vidDesc' + i );
        desc.setAttribute('id', 'vidDescId' + i);
        desc.setAttribute('class', 'form-control')
        
        if(!document.getElementById('vidNameId' + i)) {
        	divs[i].appendChild(lab); 
        	divs[i].appendChild(another); 
        }
         if(!document.getElementById('vidFileId' + i)) {
        	divs[i].appendChild(labFile); 
        	divs[i].appendChild(file); 
        }
         if(!document.getElementById('vidDescId' + i)) {
        	divs[i].appendChild(labDesc); 
        	divs[i].appendChild(desc); 
        }
    }
}


function addDivFile() {
	var div = document.createElement('DIV');
    div.setAttribute('class', 'fileClass form-group');

	var tit = document.createTextNode('Add another Document')
    div.appendChild(tit);
    var dm = document.getElementById('bodyFile');
    dm.appendChild(div);
}
function addElementsFile() {
	var divs = document.getElementsByClassName('fileClass');
    var lngt = divs.length;
    for(var i = 0; i < lngt; i++) {
    	var lab = document.createElement('LABEL');
        var x = document.createTextNode('Document Name:');
        lab.appendChild(x);
    	var another = document.createElement('INPUT');
        another.setAttribute('type', 'text');
        another.setAttribute('name', 'docName' + i);
        another.setAttribute('id', 'docNameId' + i);
        another.setAttribute('class', 'form-control')
        
        var labFile = document.createElement('LABEL');
        var y = document.createTextNode('Chose Document for upload:');
        labFile.appendChild(y);
        var file = document.createElement('INPUT');
        file.setAttribute('type', 'file');
        file.setAttribute('name', 'docFile' + i );
        file.setAttribute('id', 'docFileId' + i);
        file.setAttribute('class', 'form-control')
        
        var labDesc = document.createElement('LABEL');
        var z = document.createTextNode('Description:');
        labDesc.appendChild(z);
        var desc = document.createElement('TEXTAREA');
        desc.setAttribute('name', 'docDesc' + i );
        desc.setAttribute('id', 'docDescId' + i);
        desc.setAttribute('class', 'form-control')
        
        if(!document.getElementById('docNameId' + i)) {
        	divs[i].appendChild(lab); 
        	divs[i].appendChild(another); 
        }
         if(!document.getElementById('docFileId' + i)) {
        	divs[i].appendChild(labFile); 
        	divs[i].appendChild(file); 
        }
         if(!document.getElementById('docDescId' + i)) {
        	divs[i].appendChild(labDesc); 
        	divs[i].appendChild(desc); 
        }
    }
}


function addDivMsc() {
	var div = document.createElement('DIV');
    div.setAttribute('class', 'mscClass form-group');

	var tit = document.createTextNode('Add another Song')
    div.appendChild(tit);
    var dm = document.getElementById('bodyMusic');
    dm.appendChild(div);
}
function addElementsMsc() {
	var divs = document.getElementsByClassName('mscClass');
    var lngt = divs.length;
    for(var i = 0; i < lngt; i++) {
    	var lab = document.createElement('LABEL');
        var x = document.createTextNode('Song\s Name:');
        lab.appendChild(x);
    	var another = document.createElement('INPUT');
        another.setAttribute('type', 'text');
        another.setAttribute('name', 'mscName' + i);
        another.setAttribute('id', 'mscNameId' + i);
        another.setAttribute('class', 'form-control')
        
        var labFile = document.createElement('LABEL');
        var y = document.createTextNode('Chose Song for upload:');
        labFile.appendChild(y);
        var file = document.createElement('INPUT');
        file.setAttribute('type', 'file');
        file.setAttribute('name', 'mscFile' + i );
        file.setAttribute('id', 'mscFileId' + i);
        file.setAttribute('class', 'form-control')
        
        var labDesc = document.createElement('LABEL');
        var z = document.createTextNode('Description:');
        labDesc.appendChild(z);
        var desc = document.createElement('TEXTAREA');
        desc.setAttribute('name', 'mscDesc' + i );
        desc.setAttribute('id', 'mscDescId' + i);
        desc.setAttribute('class', 'form-control')
        
        if(!document.getElementById('mscNameId' + i)) {
        	divs[i].appendChild(lab); 
        	divs[i].appendChild(another); 
        }
         if(!document.getElementById('mscFileId' + i)) {
        	divs[i].appendChild(labFile); 
        	divs[i].appendChild(file); 
        }
         if(!document.getElementById('mscDescId' + i)) {
        	divs[i].appendChild(labDesc); 
        	divs[i].appendChild(desc); 
        }
    }
}
</script>
@endsection


