<table id="messages-table">
					@for($i = 0; $i < $messages -> count(); $i++)
						@if($messages[$i] -> show_sender == 1)
							<input id type="hidden" name="{{$messages[$i] -> id}}" >
							<tr id="{{$messages[$i] -> id}}">
								<td><input id="{{$messages[$i] -> id}}" name="delTrash-check" type="checkbox" value="1" /></td>
								<td>
									@if($messages[$i] -> readed_sender == 0)
										<div class="not-readed">
											{{$messages[$i] -> title}}
										</div>
									@else
										{{$messages[$i] -> title}}
									@endif
								</td>
								<td>
									@if($messages[$i] -> readed_sender == 0)
										<div class="not-readed">
											<?php echo $short[$i]['messageBold']; ?>
										</div>
									@else
										<?php echo $short[$i]['message']; ?>
									@endif
								</td>
								<td>{{$dt[$i]}}</td>
							</tr>
						@endif
					@endfor
				</table>
