@extends('layouts.master')

@section('title')
Read Message 
@endsection

@section('content')
	
<div class="container">

	@if(count($errors) > 0)
		<div class="row">
			<div class="col-md-3 col-lg-3">
				<ul>
					@foreach($errors -> all() as $error)
						<li>{{$error}}</li>
					@endforeach
				</ul>
			</div>
		</div>
	@endif
	@if(session('success'))
		<div class="row">
			<div class="col-md-3 col-lg-3">
				{{session('success')}}
			</div>
		</div>
	@endif
	@if(session('error'))
		<div class="row">
			<div class="col-md-3 cl-lg-3">
				{{session('error')}}
			</div>
		</div>
	@endif
	
	<div id="demo"></div>
	
	<div class="modal fade" id="imgModal">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
	      </div>
	      <div class="modal-body">
	      		<div class="row"> 
	      			<div class="col-md-4 col-lg-4">
			      		<div class="form-group">
				      		<label>Select image file:</label>
				        	<input type="file" id="img-file" name="imgFile" onchange="prevImage(this);" />
			        	</div>
		        	</div>
		        	<div class="col-md-4 col-lg-4">
			        	<label>Deffine size: <small>Max allowed is 320x200</small></label>
			        	<div class="form-group">
			        		<input id="wdt" type="text" size="2" name="width" /> x <input id="hgh" type="text" size="2" name="height" />
			      		</div>
		      		</div>
		      		<div class="col-md-4 col-lg-4">
		      			<label>Image position:</label>
		      			<div class="form-group">
		      				<input type="radio" name="pos" value="up">Above Text
		      			</div>
		      			<div class="form-group">
		      				<input type="radio" name="pos" value="left">Left of text
		      			</div>
		      			<div class="form-group">
		      				<input type="radio" name="pos" value="right">Right of text
		      			</div>
		      			<div class="form-group">
		      				<input type="radio" name="pos" value="down">Benith Text
		      			</div>
		      			<div class="form-group">
		      				<input type="radio" name="pos" value="none">As it is
		      			</div>
		      		</div>
	      		</div>
	      		<div class="row">
	      			<div class="col-md-12 col-lg-12 col-md-push-3 col-lg-push-3">
	      				<img src="" id="previewImg" alt="Your image:" />
	      			</div>
	      		</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-primary" onclick="addImage()">Add Image</button>
	      </div>
	    </div>
	  </div>
	</div>
	
	
	<div class="row">
		<div class="option-buttons">
			<div class="col-md-3 col-lg-3">
				<a href="{{route('message.singleDelete')}}/<?php echo $message[0] -> id; ?>/show_reciver" class="btn btn-default" >Delete</a>
				<a href="{{route('message.singleTrash')}}/<?php echo $message[0] -> id; ?>/trashed_reciver" class="btn btn-default" >Move to trash</a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-10 col-lg-10">
			<div class="messages-title-wrapper">
				{{$message[0] -> title}}
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4 col-lg-4">
			<div class="messages-fromTo-wrapper">
				From: {{$message[0] -> from}} To: {{$message[0] -> to}}
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-10 col-lg-10">
			<div class="message-wraper">
				 <?php echo $decoded[0]; ?> 
			</div>
		</div>
	</div>
	@if($mode == "readed_reciver")
		<div class="row">
			<div class="col-md-10 col-lg-10">
				<label>Replay</label>
				<div class="form-group">
					<div class="form-group">
						<div class="form-control">From: {{$message[0] -> from}}</div>
					</div>
					<div class="form-group">
						<div class="form-control">To: {{$message[0] -> to}}</div>
					</div>
					<div class="form-group">
						<div class="form-control">Title: {{$message[0] -> title}}</div>
					</div>
					<button id="img-btn" class="btn btn-defaul" data-toggle="modal" data-target="#imgModal" >Image</button>
					<button id="bold-btn" class="btn btn-defaul" value="b"  onclick="document.execCommand('bold',false,null); focusMessage();" >Bold</button>
					<button id="italic-btn" class="btn btn-defaul" value="i"  onclick="document.execCommand('italic',false,null); focusMessage();" >Italic</button>
					<button id="under-btn" class="btn btn-defaul" value="u"  onclick="document.execCommand('underline',false,null); focusMessage();" >Underline</button>
					<select id="font-type" onchange="changeFontType(this); focusMessage();">
						<option value="tnr">Times New Roman</option>
						<option value="g">Georgia</option>
						<option value="ah">Arial, Helvetica</option>
						<option value="ab">Arial Black</option>
						<option value="csm">Comic Sans MS</option>
						<option value="cn">Courier New</option>
					</select>
					<select id="font-size" onchange="changeFontSize(this); focusMessage();">
						<option value="1">7</option>
						<option value="2">9</option>
						<option value="3" selected="selected">12</option>
						<option value="4">14</option>
						<option value="5">16</option>
						<option value="6">18</option>
						<option value="7">20</option>
					</select>
					<form action="{{route('message.replay')}}" method="post" id="f">
						<input type="hidden" name="_token" value="<?php echo csrf_token() ?>" />
						<!-- <textarea class="form-control" id="replyArea" name="reply" rows="5" cols="10"></textarea> -->
						<div id="textarea" contentEditable="true"></div>
						<button type="button" id="{{$message[0] -> title}}" name="{{$message[0] -> from}}" value="{{$message[0] -> to}}" class="btn btn-primary" onclick="replySend(this.name, this.value, this.id);">Reply</button>
						<button type="button" class="btn btn-primary" onclick="cl();">Clear</button>
					</form>
				</div>
 			</div>
		</div>
	@endif
</div>	
{!! Html::script('ckeditor/ckeditor.js') !!}

<script>
	var img;

	var bold = 0;
	var italic = 0;
	var underline = 0;

	var cbold = 0;
	var citalic = 0;
	var cunder = 0;
	
	var message = document.getElementById('textarea');

	
	message.onkeyup = function(event) {
			
				//alert(bold);
				//alert(cbold);
			
			if(bold == 1) {
				alert("here");
				if(cbold == 0) {
					var start = message.innerHTML.length - 1;
					var end = message.innerHTML.length;
					var text = message.innerHTML.substring(start, end);
					message.innerHTML = message.innerHTML.substring(0, start) + '<b>' + text + '</b>';
				} else {
					var start = message.innerHTML.length - 8;
					var end = message.innerHTML.length;
					var text = message.innerHTML.substring(start, end);
					text =  $(str).text();
					message.innerHTML = message.innerHTML.substring(0, start) + '<b>' + text + '</b>';
	
				}
				cbold++;
			}


			if(italic == 1) {
				alert('here italic');
				if(citalic == 0) {
					var start = message.innerHTML.length - 1;
					var end = message.innerHTML.length;
					var text = message.innerHTML.substring(start, end);
					message.innerHTML = message.innerHTML.substring(0, start) + '<i>' + text + '</i>';
				} else {
					var start = message.innerHTML.length - 8;
					var end = message.innerHTML.length;
					var text = message.innerHTML.substring(start, end);
					text =  $(str).text();
					message.innerHTML = message.innerHTML.substring(0, start) + '<i>' + text + '</i>';
	
				}

				citalic++;
			}

			if(underline == 1) {

			}
			//alert(message.innerHTML);
		
	}

	function focusMessage() {
		document.getElementById('textarea').focus();
	}
	
	function prevImage(input) {

		if(input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function(event) {
				var img = document.getElementById('previewImg');
				if(img.width <= 320 && img.height <= 200) {
					img.setAttribute('src', event.target.result);
					window.img = img;
				}
			}

			reader.readAsDataURL(input.files[0]);
		}

	}


	function addImage() {

		var pos = document.getElementsByName('pos');
		var newWidth = document.getElementById('wdt').value;
		var newHeight = document.getElementById('hgh').value;

		img.height = newHeight;
		img.width = newWidth; 

		for(var i = 0; i < pos.length; i++) {

			if(pos[i].checked) {
				if(pos[i].value == "up") {
					var table = document.createElement('table');
					table.style.width = "100%";
					var tbody = document.createElement('tbody');

					var tr1 = document.createElement('tr');
					var td1 = document.createElement('td');
					td1.innerHTML = "<img src='" + img.src + "' width='" + img.width + "'height='" + img.height  + "'>";
					
					var tr2 = document.createElement('tr');
					var td2 = document.createElement('td');
					td2.innerHTML = message.innerHTML;

					tr1.appendChild(td1);
					tr2.appendChild(td2);

					tbody.appendChild(tr1);
					tbody.appendChild(tr2);

					table.appendChild(tbody);

					message.innerHTML = "";
					message.appendChild(table);
				}
				if(pos[i].value == "left") {

					/* div1 = document.createElement('SPAN');
					div1.setAttribute('id', 'texarea-chunck-side-left');
					div2 = document.createElement('SPAN');
					div2.setAttribute('id', 'texarea-chunck-side-right');

					div1.innerHTML = "<img src='" + img.src + "' width='" + img.width + "'height='" + img.height  + "'>";
					div2.innerHTML = message.innerHTML;

					message.innerHTML = "";
					message.appendChild(div1);
					message.appendChild(div2); */


					var table = document.createElement('table');
					table.style.width = "100%";
					var tbody = document.createElement('tbody');

					var tr = document.createElement('tr');
					
					var td1 = document.createElement('td');
					td1.innerHTML = "<img src='" + img.src + "' width='" + img.width + "'height='" + img.height  + "'>";
					var td2 = document.createElement('td');
					td2.innerHTML = message.innerHTML;

					tr.appendChild(td1);
					tr.appendChild(td2);

					tbody.appendChild(tr);

					table.appendChild(tbody);

					message.innerHTML = "";
					message.appendChild(table);
					
					
					
				}
				if(pos[i].value == "right") {

					var table = document.createElement('table');
					table.style.width = "100%";
					var tbody = document.createElement('tbody');

					var tr = document.createElement('tr');
					
					var td1 = document.createElement('td');
					td1.innerHTML = message.innerHTML;
					var td2 = document.createElement('td');
					td2.innerHTML = "<img src='" + img.src + "' width='" + img.width + "'height='" + img.height  + "'>";

					tr.appendChild(td1);
					tr.appendChild(td2);

					tbody.appendChild(tr);

					table.appendChild(tbody);

					message.innerHTML = "";
					message.appendChild(table);
					
				}
	
				
				if(pos[i].value == "down") {

					var table = document.createElement('table');
					table.style.width = "100%";
					var tbody = document.createElement('tbody');

					var tr1 = document.createElement('tr');
					var td1 = document.createElement('td');
					td1.innerHTML = message.innerHTML;
					
					var tr2 = document.createElement('tr');
					var td2 = document.createElement('td');
					td2.innerHTML = "<img src='" + img.src + "' width='" + img.width + "'height='" + img.height  + "'>";

					tr1.appendChild(td1);
					tr2.appendChild(td2);

					tbody.appendChild(tr1);
					tbody.appendChild(tr2);

					table.appendChild(tbody);

					message.innerHTML = "";
					message.appendChild(table);
				}

				if(pos[i].value == "none") {
					message.innerHTML = message.innerHTML + "<img src='" + img.src + "' width='" + img.width + "'height='" + img.height  + "'>";
					
				}
				
			}

		}
		//message.innerHTML = message.innerHTML + "<img src='" + img.src + "' width='" + img.width + "'height='" + img.height  + "'>";
		
		
		
	}


	function makeBold() {
		if(document.getElementById('bold-btn').value == "b") {
			window.bold = 1;
			document.getElementById('bold-btn').value = "ub";
		} else {
			window.bold = 0;
			window.cbold = 0;
			document.getElementById('bold-btn').value = "b";
		}
	} 

	function makeItalic() {
		if(document.getElementById('italic-btn').value == "i") {
			window.italic = 1;
			document.getElementById('italic-btn').value = "ui";
		} else {
			window.italic = 0;
			window.citalic = 0;
			document.getElementById('italic-btn').value = "i";
		}
	}

	function makeUnderline() {
		if(document.getElementById('under-btn').value == "u") {
			window.underline = 1;
			document.getElementById('under-btn').value = "uu";
		} else {
			window.underline = 0;
			window.cunder = 0;
			document.getElementById('under-btn').value = "u";
		}
	}


	function changeFontType(input) {
		var fontType = input.value;
		if(fontType == "tnr") {
			message.style.fontFamily = "'Times New Roman', Times, serif";
		} 
		if(fontType == "g") {
			message.style.fontFamily = "Georgia, serif";	
		}
		if(fontType == "ab") {
			message.style.fontFamily = "'Arial Black', Gadget, sans-serif";	
		}
		if(fontType == "ah") {
			message.style.fontFamily = "Arial, Helvetica, sans-serif";	
		}
		if(fontType == "csm") {
			message.style.fontFamily = "'Comic Sans MS', cursive, sans-serif";	
		}
		if(fontType == "cn") {
			message.style.fontFamily = "'Courier New', Courier, monospace";	
		}
	}

	function changeFontSize(input) {
		document.execCommand('fontSize',false, parseInt(input.value));
	}
	
	function cl() {
		message.innerHTML = "";
	}


	function replySend(from, to, title) {
		var messageSend = message.innerHTML;
		var messageEncoded = window.btoa(messageSend);
		var send = JSON.stringify([from, to, title, messageEncoded]);
		//var send = JSON.stringify(sendRaw);
		alert(send);
		
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if(this.readyState == 4 && this.status == 200) { 

			var res = this.responseText;
			alert(res);
			}
		}
		xhttp.open("GET", "{{route('message.replay')}}" + "/" + send, true);
		xhttp.send();

	}
	
	CKEDITOR.disableAutoInline = true;
</script>

<script>
//CKEDITOR.replace( 'replyArea' );
</script>	
@endsection






















