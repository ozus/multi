@extends('layouts.master')

@section('title')
Sent
@endsection

@section('content')

<div class="container">
<div id="demo"></div>
	<div class="row">
		<div class="col-md-3 col-lg-3">
			<div class="alert" id="notify" style="display: none" role="alert">
				<div id="notification"></div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="option-buttons">
			<div class="col-lg-3 col-md-3">
				<button  type="button" class="btn btn-default" onclick="del();">Delete</button>
				<button  type="button" class="btn btn-default" onclick="toTrash();">Move to trash</button>
				<select id="recivers" name="recivers-list" class="" onchange="getRecivers()">
					<option value="all" selected="selected">all</option>
					@for($i = 0; $i < count($recivers); $i++)
						<option value="{{$recivers[$i]}}">{{$recivers[$i]}}</option>
					@endfor
				</select>
			</div>	
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 col-lg-12">
			<div class="inbox-table">
				<table id="messages-table">
					@for($i = 0; $i < $messages -> count(); $i++)
						@if($messages[$i] -> show_sender == 1)
							<input id type="hidden" name="{{$messages[$i] -> id}}" >
							<tr id="{{$messages[$i] -> id}}">
								<td><input id="{{$messages[$i] -> id}}" name="delTrash-check" type="checkbox" value="1" /></td>
								<td>
									@if($messages[$i] -> readed_sender == 0)
										<div class="not-readed">
											{{$messages[$i] -> title}}
										</div>
									@else
										{{$messages[$i] -> title}}
									@endif
								</td>
								<td>
									@if($messages[$i] -> readed_sender == 0)
										<div class="not-readed">
											<?php echo $short[$i]['messageBold']; ?>
										</div>
									@else
										<?php echo $short[$i]['message']; ?>
									@endif
								</td>
								<td>{{$dt[$i]}}</td>
							</tr>
						@endif
					@endfor
				</table>
			</div>
		</div>
	</div>
</div>


<script>
		
	var table = document.getElementById('messages-table');
	var recivers = document.getElementById('recivers');
	
	for(var i = 0; i < table.rows.length; i++) {

		for(var j = 1; j < table.rows[i].cells.length; j++) {

				var currentCell = table.rows[i].cells[j];

				currentCell.onclick = function() {

					window.location.assign("{{route('message.read')}}/" + this.parentNode.id + "/readed_sender");

				}
		}

	}


	function del() {

		var chck = document.getElementsByName("delTrash-check");
		var mode = "show_sender";
		var lng = chck.length;

		for(var i = 0; i < lng; i++) {
			if(chck[i].checked) {

		
				var xhttp = new XMLHttpRequest();
				xhttp.onreadystatechange = function() {
					if(this.readyState == 4 && this.status == 200) {
						document.getElementById('demo').innerHTML = this.responseText;

						$("#notification").html(this.responseText);
						$("#notify").removeClass;
						$("#notify").addClass('alert alert-success');
						$("#notify").show().delay(5000).fadeOut();

						
					}
				}
				xhttp.open("GET", "{{route('message.delete')}}" + "/" + chck[i].id + "/" + mode, true);	
				xhttp.send(); 

				table.deleteRow(i);
			}

		}
	}


	
	function toTrash() {
		var chck = document.getElementsByName("delTrash-check");
		var lng = chck.length;
		var mode = "trashed_sender";
		
		for(var i = 0; i < lng; i++) {
			if(chck[i].checked) {
				
				var xhttp = new XMLHttpRequest();
				xhttp.onreadystatechange = function() {
					if(this.readyState == 4 && this.status == 200) {
						document.getElementById('demo').innerHTML = this.responseText;

						$("#notification").html(this.responseText);
						$("#notify").removeClass;
						$("#notify").addClass('alert alert-success');
						$("#notify").show().delay(5000).fadeOut();

					}
				}
				xhttp.open("GET", "{{route('message.toTrash')}}" + "/" + chck[i].id + "/" + mode, true);	
				xhttp.send();

				table.deleteRow(i);
			}
		}
	}


	function getRecivers() {

		var reciver = recivers.value;

		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if(this.readyState == 4 && this.status == 200) {
				document.getElementsByClassName('inbox-table')[0].innerHTML = this.responseText;
				window.table = document.getElementById('messages-table');
			}
		}
		xhttp.open("GET", "{{route('message.getRecivers')}}" + "/" + reciver, true);	
		xhttp.send();
		
	}

</script>

@endsection








