@extends('layouts.master')

@section('title')
	
@endsection

@section('content')

	<div class="container">
		<div class="col-md-12 col-lg-12 col-lg-push-2" >
			<div class="no-content-picture">
				<img src="{{url('/')}}/img/no-content.jpg" width="800" height="700" />
			</div>
			<div class="profile-no-content">
				No content here, maybe you need to upload some files?
			</div>
		</div>
	</div>
	

@endsection