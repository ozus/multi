<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\MyOwnResetPassword as ResetPasswordNotification;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'hashed_password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'hashed_password'
    ];
    
    public function getAuthPassword()
    {
    	return $this->hashed_password;
    }
    
    public function getActive() {
    	
    	return $this -> active;
    	
    }
    
    
    public function sendPasswordResetNotification($token)
    {	
    	
    	$this->notify(new ResetPasswordNotification($token));
    }
    
    
    public function file() {
    		
    	return $this -> hasMany('App\File', 'usr_id');
    	
    }
    public function comment() {
    	
    	return $this -> hasMany('App\Comment', 'usr_id');
    	
    }
    public function favorits() {
    	
    	return $this -> belongsToMany('App\Favorit', 'users_favorits', 'usr_id', 'fav_id');
    	
    }
}
