<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class StatisticsController extends Controller
{
	
	
    public function video(Request $request) {

    	$videos = DB::table('files') -> select('*') -> where('path', 'LIKE', '%video%') -> limit(6) -> get();
    	$videosCount = DB::table('files') -> select('*') -> where('path', 'LIKE', '%video%') -> get();
    	$videosStats = DB::table('files') -> select('*') -> where('path', 'LIKE', '%video%') -> paginate(3);
    	
    	//totals
    	$totals = $this -> makeTotal($videosCount);
    		
    	
    	//creating liked, disliked and viewed arrays
    	$chartData = $this -> makeDataArrays($videos);
    	
    	
    	//make most liked chart
    	$likedStr = $this -> makeChart($chartData[0], $totals[0]);
    	
    	//make most disliked chart
    	$dislikedStr = $this -> makeChart($chartData[1], $totals[1]);
    	
    	//make most viewed chart
    	$viewedStr = $this -> makeChart($chartData[2], $totals[2]);
		
		//$url = $request -> url();
		
		$title = substr($videos[0] -> path, 12, 6);
		$titleFormated = ucfirst($title);
		
		return view('pages.statistics', 
				['liked' => $likedStr, 'dis' => $dislikedStr, 'viewed' => $viewedStr, 'title' => $titleFormated, 'videos' => $videosStats]);
    	
	}
	
	
	
	public function image(Request $request) {
	
		$videos = DB::table('files') -> select('*') -> where('path', 'LIKE', '%images%') -> limit(6) -> get();
		$videosCount = DB::table('files') -> select('*') -> where('path', 'LIKE', '%images%') -> get();
		$videosStats = DB::table('files') -> select('*') -> where('path', 'LIKE', '%images%') -> paginate(3);
		 
		//totals
		$totals = $this -> makeTotal($videosCount);
		
		//creating liked, disliked and viewed arrays
		$chartData = $this -> makeDataArrays($videos);
		 
		
		 
		//make most liked chart
		$likedStr = $this -> makeChart($chartData[0], $totals[0]);
		 
		//make most disliked chart
		$dislikedStr = $this -> makeChart($chartData[1], $totals[1]);
		 
		//make most viewed chart
		$viewedStr = $this -> makeChart($chartData[2], $totals[2]);
		
		//$url = $request -> url();
		
		$title = substr($videos[0] -> path, 12, 6);
		$titleFormated = ucfirst($title);
	
		return view('pages.statistics',
				['liked' => $likedStr, 'dis' => $dislikedStr, 'viewed' => $viewedStr, 'title' => $titleFormated, 'videos' => $videosStats]);
		 
	}
	
	
	public function music(Request $request) {
	
		$videos = DB::table('files') -> select('*') -> where('path', 'LIKE', '%music%') -> limit(6) -> get();
		$videosCount = DB::table('files') -> select('*') -> where('path', 'LIKE', '%music%') -> get();
		$videosStats = DB::table('files') -> select('*') -> where('path', 'LIKE', '%music%') -> paginate(3);
			
		//totals
		/* $likeTotal = array();
		$disTotal = array();
		$viewedTotal = array();
			
		foreach($videosCount as $vid) {
			$likeTotal[$vid -> user_name] = $vid -> liked;
			$disTotal[$vid -> user_name] = $vid -> disliked;
			$viewedTotal[$vid -> user_name] = $vid -> viewed;
		}
			
		$sumLiked = array_sum($likeTotal);
		$sumDis = array_sum($disTotal);
		$sumViewed = array_sum($viewedTotal); */
		
		$totals = $this -> makeTotal($videosCount);
	
			
		//creating liked, disliked and viewed arrays
		/* $liked = array();
		$disliked = array();
		$viewed = array();
			
		foreach($videos as $video) {
			if($video -> liked > 0) {
				$liked[$video -> user_name] = $video -> liked;
			}
			if($video -> disliked > 0) {
				$disliked[$video -> user_name] = $video -> disliked;
			}
			if($video -> viewed > 0) {
				$viewed[$video -> user_name] = $video -> viewed;
			}
		} */
		
		$chartData = $this -> makeDataArrays($videos);
		
		//make most liked chart
		$likedStr = $this -> makeChart($chartData[0], $totals[0]);
	
		//make most disliked chart
		$dislikedStr = $this -> makeChart($chartData[1], $totals[1]);
	
		//make most viewed chart
		$viewedStr = $this -> makeChart($chartData[2], $totals[2]);
	
		//$url = $request -> url();
	
		$title = substr($videos[0] -> path, 12, 5);
		$titleFormated = ucfirst($title);
	
		return view('pages.statistics',
				['liked' => $likedStr, 'dis' => $dislikedStr, 'viewed' => $viewedStr, 'title' => $titleFormated, 'videos' => $videosStats]);
			
	}
	
	
	public function ajaxDetailsShow() {
		
		$button = "<button class='btn btn-default' type='button' onclick='hidee()' >Hide</button>";
		
		return $button;
		
	}
	
	
	public function ajaxDetailsHide() {
		
		$button = "<button class='btn btn-default' type='button' onclick='showw()' >Show</button>";
		
		return $button;
		
	}
	
	
	protected function makeTotal($count) {
		
		$likeTotal = array();
		$disTotal = array();
		$viewedTotal = array();
			
		foreach($count as $ct) {
			$likeTotal[$ct -> user_name] = $ct -> liked;
			$disTotal[$ct -> user_name] = $ct -> disliked;
			$viewedTotal[$ct -> user_name] = $ct -> viewed;
		}
			
		$sumLiked = array_sum($likeTotal);
		$sumDis = array_sum($disTotal);
		$sumViewed = array_sum($viewedTotal);
		
		$returnArray = array($sumLiked, $sumDis, $sumViewed);
		
		return $returnArray;
		
	}
	
	
	protected function makeDataArrays($data) {
		
		$liked = array();
		$disliked = array();
		$viewed = array();
			
		foreach($data as $dat) {
			if($dat -> liked > 0) {
				$liked[$dat -> user_name] = $dat -> liked;
			}
			if($dat -> disliked > 0) {
				$disliked[$dat -> user_name] = $dat -> disliked;
			}
			if($dat -> viewed > 0) {
				$viewed[$dat -> user_name] = $dat -> viewed;
			}
		}
		
		$returnArray = array($liked, $disliked, $viewed);
		
		return $returnArray;
		
	}
	
	protected function makeChart($data, $total) {
		
		$colors = array('#8B0000', '#00CED1', '#228B22', '#FFD700', '#D3D3D3', '#708090');
		
		asort($data);
		$counter = 0;
		
		$lng = count($data);
		for($i = 0; $i < $lng; $i++) {
			$counter++;
		}
		
		$sum = array_sum($data);
		$strChart = "";
		$colorCount = 0;
		foreach($data as $name => $value) {
			if($counter > 0 && $total > 0) {
				$percent = round(($value/$total) * 100);
			}
			
			$strChart = $strChart 
			. "<p><table>
					<tr>
						<td width='120px'>" . $name . "</td>
						<td width='" . $value  . "px' bgcolor='" . $colors[$colorCount] ."'></td>
						<td>" . '   ' . $percent . "%</td>		
					</tr>	
			   </table></p>";
			$colorCount++;
		}
		
		return $strChart;
		
	}
}
