<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use Auth;
use Storage;
use App\File as Fil;

class UploadController extends Controller
{
    public function __construct() {
    
    	//$this -> middleware('auth');

	}
	
	public function getUpload() {
		
		return view('pages.upload');
		
	}
	
	public function postUpload(Request $request) {
		
		
		$exist = array();
		$error = 0;
		
		
		if($request -> has('imgName')) {
		
			
			$allowedExt = array('jpg', 'jpeg', 'png', 'bmp', 'gif');
			
			$data = $request -> toArray();
				
			foreach($data as $key => $value) {
				if(stripos($key, 'img') !== false) {
					$imgData[$key] = $value;
				}
			}
			
			
			
			
			//adding extantion and file name to image data
			$n = count($imgData);
			for($i = 0; $i < $n/3 - 1; $i++) {
				
				$extention = $imgData['imgFile' . $i] -> getClientOriginalExtension();
				$name = $imgData['imgFile' . $i] -> getClientOriginalName();
			
				//first error checker
				if(!in_array($extention, $allowedExt)) {
					$request -> session() -> flash('error', 'Extention of image file' . ' ' . $imgData['imgName' . $i] . ' ' . 'is not suported');
					return redirect() -> route('upload');
				}
				
				$imgData['extention' . $i] = $extention;
				$imgData['name' . $i] = $name;
				
				$this -> makeThumb($imgData['imgFile' . $i], $imgData['name' . $i], $extention);
				$this -> makeLarge($imgData['imgFile' . $i], $imgData['name' . $i], $extention);
				
			}
			$extention = $imgData['imgFile'] -> getClientOriginalExtension();
			$name = $imgData['imgFile'] -> getClientOriginalName();
	
			//second error checker
			if(!in_array($extention, $allowedExt)) {
				$request -> session() -> flash('error', 'Extention of image file' . $imgData['imgName'] . 'is not suported');
				return redirect() -> route('upload');
			}
	
			$imgData['extention'] = $extention;
			$imgData['name'] = $name;
			var_dump($imgData);
		
			
			//making neccessery image sizes
			$this -> makeThumb($imgData['imgFile'], $imgData['name'], $extention);
			$this -> makeLarge($imgData['imgFile'], $imgData['name'], $extention);
			
			

			
			
			
			//storing img files
			$path = public_path('images');
			$pathThumb = public_path('thumbs');
			$nameNoExt = str_replace("." .$extention, '', $name);
				//checking if img in storage exsists
			if(!File::exists(storage_path('uploads/images/' . $nameNoExt))) {
			$pathStore = $imgData['imgFile'] -> storeAs('uploads/images', $nameNoExt);
			} else {
				$error = 1;
				array_push($exist, $imgData['name']);
				
			}
				//checkin if public img exists
			if(!File::exists(public_path('images/' . $imgData['name']))) {
				$imgData['imgFile'] -> move($path, $imgData['name']);
			} else {
				$error = 1;
				array_push($exist, $imgData['name']);
			}
			
			for($j = 0; $j < $n/3 - 1; $j++) {
				$nameNoExt1 = str_replace("." .$imgData['extention' . $j], "", $imgData['name' . $j]);
				
				if(!File::exists(storage_path('uploads/images/' . $nameNoExt1))) {
					$pathStore = $imgData['imgFile' . $j] -> storeAs('uploads/images', $nameNoExt1);
				} else {
					$error = 1;
					array_push($exist, $imgData['name'. $j]);
				}
				if(!File::exists(public_path('images/' . $imgData['name' . $j]))) {
					$imgData['imgFile' . $j] -> move($path, $imgData['name' . $j]);
				} else {
					$error = 1;
					array_push($exist, $imgData['name' . $j]);
				}	
			}
			
			
			
			
			
			
			//adding data about image in database
			if(!Fil::where('name', '=', $nameNoExt) -> exists()) {
				$file = new Fil();
				
				$file -> usr_id = Auth::user() -> id;
				$file -> path = 'app/uploads/images/' . $nameNoExt;
				$file -> name = $nameNoExt;
				$file -> extention = $imgData['extention'];
				$file -> desc = $imgData['imgDesc'];
				$file -> user_name = $imgData['imgName'];
				$file -> user = Auth::user() -> name;
				
				$file -> save();
			} else {
				$error = 1;
			}
			for($z = 0; $z < $n/3 - 1; $z++) {
				$nameNoExt1 = str_replace("." .$imgData['extention' . $z], "", $imgData['name' . $z]);
				if(!Fil::where('name', '=', $nameNoExt1) -> exists()) {
					$file1  = new Fil();
					
					$file1  -> usr_id = Auth::user() -> id;
					$file1  -> path = 'app/uploads/images/' . $nameNoExt1;
					$file1 -> name = $nameNoExt1;
					$file1  -> extention = $imgData['extention' . $z];
					$file1  -> desc = $imgData['imgDesc' . $z];
					$file1  -> user_name = $imgData['imgName' . $z];
					$file1  -> user = Auth::user() -> name;
					
					$file1  -> save();
				} else {
					$error = 1;
				} //if exists
			}//for
			
			
		}//if img
		
		
//******************************************************************************************************************************		
		
		if($request -> has('vidName')) {
			
			
			$allowedExt = array('mp4', 'wmv', 'mkv', 'avi');
				
			$data = $request -> toArray();
			
			foreach($data as $key => $value) {
				if(stripos($key, 'vid') !== false) {
					$vidData[$key] = $value;
				}
			}
			//var_dump($vidData);
			
			
			
			
			
			//adding extantion and file name to video data
			$n = count($vidData);
			for($i = 0; $i < $n/3 - 1; $i++) {
			
				$extention = $vidData['vidFile' . $i] -> getClientOriginalExtension();
				$name = $vidData['vidFile' . $i] -> getClientOriginalName();
					
				//first error checker
				if(!in_array($extention, $allowedExt)) {
					$request -> session() -> flash('error', 'Extention of image file' . ' ' . $vidData['vidName' . $i] . ' ' . 'is not suported');
					return redirect() -> route('upload');
				}
			
				$vidData['extention' . $i] = $extention;
				$vidData['name' . $i] = $name;
			
			}
			$extention = $vidData['vidFile'] -> getClientOriginalExtension();
			$name = $vidData['vidFile'] -> getClientOriginalName();
			
			//second error checker
			if(!in_array($extention, $allowedExt)) {
				$request -> session() -> flash('error', 'Extention of image file' . $vidData['vidName'] . 'is not suported');
				return redirect() -> route('upload');
			}
			
			$vidData['extention'] = $extention;
			$vidData['name'] = $name;
			var_dump($vidData);
			
			
			
			
			
			
			
			//storing video files
			$path = public_path('videos');
			$nameNoExt = str_replace('.' .$extention, '', $name );
			if(!File::exists(storage_path('app/uploads/videos/' . $nameNoExt))) {
				$pathStore = $vidData['vidFile'] -> storeAs('uploads/videos', $nameNoExt);
			} else {
				$error = 1;
				array_push($exist, $vidData['name']);
			}
			
			//checkin if public video exists
			if(!File::exists(public_path('videos/' . $vidData['name']))) {
				$vidData['vidFile'] -> move($path, $vidData['name']);
			} else {
				$error = 1;
				array_push($exist, $vidData['name']);
			}
			
			for($j = 0; $j < $n/3 - 1; $j++) {
				$nameNoExt1 = str_replace('.' .$vidData['extention' . $j], '', $vidData['name' . $j]);
				if(!File::exists(storage_path('app/uploads/videos/' . $nameNoExt1))) {
					$pathStore = $vidData['vidFile' . $j] -> storeAs('uploads/videos', $nameNoExt1);
				} else {
					$error = 1;
					array_push($exist, $vidData['name' . $j]);
				}
				
				if(!File::exists(public_path('videos/' . $vidData['name' . $j]))) {
					$vidData['vidFile' . $j] -> move($path, $vidData['name' . $j]);
				} else {
					$error = 1;
					array_push($exist, $vidData['name' . $j]);
				}
			}
			
			
			
			
			
			if(!Fil::where('name', '=', $nameNoExt) -> exists()) {
				$file = new Fil();
					
				$file -> usr_id = Auth::user() -> id;
				$file -> path = 'app/uploads/videos/' . $nameNoExt;
				$file -> name = $nameNoExt;
				$file -> extention = $vidData['extention'];
				$file -> desc = $vidData['vidDesc'];
				$file -> user_name = $vidData['vidName'];
				$file -> user = Auth::user() -> name;
					
				$file -> save();
			} else {
				$errorFile = 1;
			}
			for($z = 0; $z < $n/3 - 1; $z++) {
				$nameNoExt1 = str_replace("." .$vidData['extention' . $z], "", $vidData['name' . $z]);
				if(!Fil::where('name', '=', $nameNoExt1) -> exists()) {
					$file1  = new Fil();
			
					$file1  -> usr_id = Auth::user() -> id;
					$file1  -> path = 'app/uploads/videos/' . $nameNoExt1;
					$file1 -> name = $nameNoExt1;
					$file1  -> extention = $vidData['extention' . $z];
					$file1  -> desc = $vidData['vidDesc' . $z];
					$file1  -> user_name = $vidData['vidName' . $z];
					$file1  -> user = Auth::user() -> name;
			
					$file1  -> save();
				} else {
					$errorFile = 1;
				} //if exists
			}//for
			
			
			
		}//if video
		
		
		
		
//******************************************************************************************************************************
		
		if($request -> has('docName')) {
			
			
			
			
			$allowedExt = array('pdf', 'txt', 'docx');
			
			$data = $request -> toArray();
				
			foreach($data as $key => $value) {
				if(stripos($key, 'doc') !== false) {
					$fileData[$key] = $value;
				}
			}
			echo "<br><br>";
			var_dump($fileData);
			echo "<br><br>";
			
			
			
			
			
			//adding extantion and file name to document data
			$n = count($fileData);
			echo $n;
			for($i = 0; $i < $n/3 - 1; $i++) {
			
				$extention = $fileData['docFile' . $i] -> getClientOriginalExtension();
				$name = $fileData['docFile' . $i] -> getClientOriginalName();
					
				//first error checker
				if(!in_array($extention, $allowedExt)) {
					$request -> session() -> flash('error', 'Extention of Document file' . ' ' . $fileData['docName' . $i] . ' ' . 'is not suported');
					return redirect() -> route('upload');
				}
			
				$fileData['extention' . $i] = $extention;
				$fileData['name' . $i] = $name;
			
			}
			$extention = $fileData['docFile'] -> getClientOriginalExtension();
			$name = $fileData['docFile'] -> getClientOriginalName();
			
			//second error checker
			if(!in_array($extention, $allowedExt)) {
				$request -> session() -> flash('error', 'Extention of Document file' . $fileData['docName'] . 'is not suported');
				return redirect() -> route('upload');
			}
			
			$fileData['extention'] = $extention;
			$fileData['name'] = $name;
			var_dump($fileData);
			
			
			
			
			
			
			//storing Document files
			$nameNoExt = str_replace("." .$extention, '', $name);
			//checking if img in storage exsists
			var_dump(!File::exists(storage_path('app/uploads/documents/' . $nameNoExt)));
			if(!File::exists(storage_path('app/uploads/documents/' . $nameNoExt))) {
				$pathStore = $fileData['docFile'] -> storeAs('uploads/documents', $nameNoExt);
			} else {
				$error = 1;
				array_push($exist, $fileData['name']);
			
			}
			for($j = 0; $j < $n/3 - 1; $j++) {
				$nameNoExt1 = str_replace("." .$fileData['extention' . $j], "", $fileData['name' . $j]);
			
				if(!File::exists(storage_path('app/uploads/documents/' . $nameNoExt1))) {
					$pathStore = $fileData['docFile' . $j] -> storeAs('uploads/documents', $nameNoExt1);
				} else {
					$error = 1;
					array_push($exist, $fileData['name'. $j]);
				}
			}
			
			
			
			
			
			//adding data about image in database
			if(!Fil::where('name', '=', $nameNoExt) -> exists()) {
				$file = new Fil();
			
				$file -> usr_id = Auth::user() -> id;
				$file -> path = 'app/uploads/documents/' . $nameNoExt;
				$file -> name = $nameNoExt;
				$file -> extention = $fileData['extention'];
				$file -> desc = $fileData['docDesc'];
				$file -> user_name = $fileData['docName'];
				$file -> user = Auth::user() -> name;
			
				$file -> save();
			} else {
				$error = 1;
			}
			for($z = 0; $z < $n/3 - 1; $z++) {
				$nameNoExt1 = str_replace("." .$fileData['extention' . $z], "", $fileData['name' . $z]);
				if(!Fil::where('name', '=', $nameNoExt1) -> exists()) {
					$file1  = new Fil();
						
					$file1  -> usr_id = Auth::user() -> id;
					$file1  -> path = 'app/uploads/documents/' . $nameNoExt1;
					$file1 -> name = $nameNoExt1;
					$file1  -> extention = $fileData['extention' . $z];
					$file1  -> desc = $fileData['docDesc' . $z];
					$file1  -> user_name = $fileData['docName' . $z];
					$file1  -> user = Auth::user() -> name;
						
					$file1  -> save();
				} else {
					$error = 1;
				} //if exists
			}//for
			
		}//if file
		
		
		
		
		
		
//******************************************************************************************************************************
		
		if($request -> has('mscName')) {
			
			
			
			$allowedExt = array('mp3', 'wav', 'wma');
				
			$data = $request -> toArray();
			
			foreach($data as $key => $value) {
				if(stripos($key, 'msc') !== false) {
					$mscData[$key] = $value;
				}
			}
			
			
			
			
			//adding extantion and file name to document data
			$n = count($mscData);
			echo $n;
			for($i = 0; $i < $n/3 - 1; $i++) {
					
				$extention = $mscData['mscFile' . $i] -> getClientOriginalExtension();
				$name = $mscData['mscFile' . $i] -> getClientOriginalName();
					
				//first error checker
				if(!in_array($extention, $allowedExt)) {
					$request -> session() -> flash('error', 'Extention of Song file' . ' ' . $mscData['mscName' . $i] . ' ' . 'is not suported');
					return redirect() -> route('upload');
				}
					
				$mscData['extention' . $i] = $extention;
				$mscData['name' . $i] = $name;
					
			}
			$extention = $mscData['mscFile'] -> getClientOriginalExtension();
			$name = $mscData['mscFile'] -> getClientOriginalName();
				
			//second error checker
			if(!in_array($extention, $allowedExt)) {
				$request -> session() -> flash('error', 'Extention of Document file' . $mscData['mscName'] . 'is not suported');
				return redirect() -> route('upload');
			}
				
			$mscData['extention'] = $extention;
			$mscData['name'] = $name;
			var_dump($mscData);
			
			
			
			
			
			
			//storing Audio files
			$path = public_path('musics');
			$nameNoExt = str_replace("." .$extention, '', $name);
			//checking if img in storage exsists
			if(!File::exists(storage_path('app/uploads/music/' . $nameNoExt))) {
				$pathStore = $mscData['mscFile'] -> storeAs('uploads/music', $nameNoExt);
			} else {
				$error = 1;
				array_push($exist, $mscData['name']);
					
			}
			if(!File::exists(public_path('musics/' . $mscData['name']))) {
				$mscData['mscFile'] -> move($path, $mscData['name']);
			} else {
				$error = 1;
				array_push($exist, $mscData['name']);
			}
			for($j = 0; $j < $n/3 - 1; $j++) {
				$nameNoExt1 = str_replace("." .$mscData['extention' . $j], "", $mscData['name' . $j]);
					
				if(!File::exists(storage_path('app/uploads/music/' . $nameNoExt1))) {
					$pathStore = $mscData['mscFile' . $j] -> storeAs('uploads/music', $nameNoExt1);
				} else {
					$error = 1;
					array_push($exist, $mscData['name'. $j]);
				}
				
				if(!File::exists(public_path('musics/' . $mscData['name' . $j]))) {
					$mscData['mscFile' . $j] -> move($path, $mscData['name' . $j]);
				} else {
					$error = 1;
					array_push($exist, $mscData['name' . $j]);
				}
			}
			
			
			
			
			
			
			//adding data about Song in database
			if(!Fil::where('name', '=', $nameNoExt) -> exists()) {
				$file = new Fil();
					
				$file -> usr_id = Auth::user() -> id;
				$file -> path = 'app/uploads/music/' . $nameNoExt;
				$file -> name = $nameNoExt;
				$file -> extention = $mscData['extention'];
				$file -> desc = $mscData['mscDesc'];
				$file -> user_name = $mscData['mscName'];
				$file -> user = Auth::user() -> name;
					
				$file -> save();
			} else {
				//$errorFile = 1;
			}
			for($z = 0; $z < $n/3 - 1; $z++) {
				$nameNoExt1 = str_replace("." .$mscData['extention' . $z], "", $mscData['name' . $z]);
				if(!Fil::where('name', '=', $nameNoExt1) -> exists()) {
					$file1  = new Fil();
			
					$file1  -> usr_id = Auth::user() -> id;
					$file1  -> path = 'app/uploads/music/' . $nameNoExt1;
					$file1 -> name = $nameNoExt1;
					$file1  -> extention = $mscData['extention' . $z];
					$file1  -> desc = $mscData['mscDesc' . $z];
					$file1  -> user_name = $mscData['mscName' . $z];
					$file1  -> user = Auth::user() -> name;
			
					$file1  -> save();
				} else {
					//$errorFile = 1;
				} //if exists
			}//for
			
			
		}//if music
		
		
		echo $error;
		if($error == 1) {
			$request -> session() -> flash('exist', $exist);
			$request -> session() -> flash('error', 'following files already exists:');
			return redirect() -> route('upload');
		} else {
			$request -> session() -> flash('success', 'Files where successfully uploaded');
			return redirect() -> route('upload');
		}
		
	}//function
	
	
	
	
	public function download($id = null) {
			
		$file = Fil::where('id', '=', $id) -> get();
		
		return response() -> download(storage_path($file[0] -> path));
		
	}
	
	
	public function show($id = null) {
		
		$file = Fil::where('id', '=', $id) -> get();
		
		return response() -> file(storage_path(($file[0] -> path)));
		
	}
	
	
	public function delete(Request $request, $id = null, $type = null) {
		
		$file = Fil::where('id', '=', $id) -> get();
		
		if(File::exists(storage_path($file[0] -> path))) {
			File::delete(storage_path($file[0] -> path));
		}
		
		
		if(File::exists(public_path('images/' . $file[0] -> name . "." . $file[0] -> extention))) {
			File::delete(public_path('images/' .$file[0] -> name . "." . $file[0] -> extention));
		}
		if(File::exists(public_path('videos/' . $file[0] -> name . "." . $file[0] -> extention))) {
			File::delete(public_path('videos/' .$file[0] -> name . "." . $file[0] -> extention));
		}
		if(File::exists(public_path('thumbs/' . $file[0] -> name . "." . $file[0] -> extention))) {
			File::delete(public_path('thumbs/' .$file[0] -> name . "." . $file[0] -> extention));
		}
		if(File::exists(public_path('large/' . $file[0] -> name . "." . $file[0] -> extention))) {
			File::delete(public_path('large/' .$file[0] -> name . "." . $file[0] -> extention));
		}
		
		if(Fil::where('id', '=', $file[0] -> id) -> exists() ) {
			Fil::where('id', '=', $file[0] -> id) -> delete();
		}
		
		$request -> session() -> flash('success', 'File ' . $file[0] -> user_name  . ' was successfuly deleted');
		
		if($type == "video") {
			return redirect() -> route('video');
		} 
		if($type == "image") {
			return redirect() -> route('image');
		}
		if($type == "document") {
			return redirect() -> route('document');
		}
		if($type == "music") {
			return redirect() -> route('music');
		}
	}
	
	
	
	protected function makeThumb($file, $name, $ext) {
		
		
		$filename = $file;
		$percent = 0.5;
			
		list($width, $height) = getimagesize($filename);
			
		$newHeight = 450;
		$newWidth = 620;
		
		$ratio_orig = $width/$height;
		
		if ($newWidth/$newHeight > $ratio_orig) {
			$newWidth = $newHeight*$ratio_orig;
		} else {
			$newHeight = $newWidth/$ratio_orig;
		}
		
		$newImg = imagecreatetruecolor($newWidth, $newHeight);
		switch ($ext) {
			case 'jpg' :
				$source = imagecreatefromjpeg($filename);
				imagecopyresized($newImg, $source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
				imagejpeg($newImg, public_path('thumbs/' . $name));
				break;
			case 'jpeg' :
				$source = imagecreatefromjpeg($filename);
				imagecopyresized($newImg, $source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
				imagejpeg($newImg, public_path('thumbs/' . $name));
				break;
			case 'png' :
				$source = imagecreatefrompng($filename);
				imagecopyresized($newImg, $source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
				imagepng($newImg, public_path('thumbs/' . $name));
				break;
			case 'bmp' :
				$source = imagecreatefromwbmp($filename);
				imagecopyresized($newImg, $source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
				imagexbm($newImg, public_path('thumbs/' . $name));
				break;
			case 'gif' :
				$source = imagecreatefromgif($filename);
				imagecopyresized($newImg, $source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
				imagegif($newImg, public_path('thumbs/' . $name));
				break;
		
		}
		
	}
	
	protected function makeLarge($file, $name, $ext) {
	
	
		$filename = $file;
			
		list($width, $height) = getimagesize($filename);
			
		$newHeight = 720;
		$newWidth = 1280;
		
		$ratio_orig = $width/$height;
		
		if ($newWidth/$newHeight > $ratio_orig) {
			$newWidth = $newHeight*$ratio_orig;
		} else {
			$newHeight = $newWidth/$ratio_orig;
		}
			
		$newImg = imagecreatetruecolor($newWidth, $newHeight);
		switch ($ext) {
			case 'jpg' :
				$source = imagecreatefromjpeg($filename);
				imagecopyresized($newImg, $source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
				imagejpeg($newImg, public_path('large/' . $name));
				break;
			case 'jpeg' :
				$source = imagecreatefromjpeg($filename);
				imagecopyresized($newImg, $source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
				imagejpeg($newImg, public_path('large/' . $name));
				break;
			case 'png' :
				$source = imagecreatefrompng($filename);
				imagecopyresized($newImg, $source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
				imagepng($newImg, public_path('large/' . $name));
				break;
			case 'bmp' :
				$source = imagecreatefromwbmp($filename);
				imagecopyresized($newImg, $source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
				imagexbm($newImg, public_path('large/' . $name));
				break;
			case 'gif' :
				$source = imagecreatefromgif($filename);
				imagecopyresized($newImg, $source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
				imagegif($newImg, public_path('large/' . $name));
				break;
	
		}
	
	}
	
	
	
}//class

















