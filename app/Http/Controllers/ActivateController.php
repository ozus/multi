<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class ActivateController extends Controller
{
    public function activate($id = null) {
    
    	User::where('id', '=', $id) -> update(['active' => 1]);
    	
    	return view('auth.activated');
    
	}
	
	public function deactivate($id = null) {
		
		User::where('id', '=', $id) -> update(['active' => 0]);
		 
		return view('auth.deactivated');
		
	}
}
