<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\File;
use Auth;

class VideoController extends Controller
{
    public function getVideo() {
    	
    	$videos = File::where('path', 'LIKE', '%videos%') -> paginate(3);
    	
    	$viewed = File::where('path', 'LIKE', '%videos%') -> where('viewed', '>', '5') -> limit(5) -> get();
    
		return view('pages.video', ['videos'=> $videos, 'viewed' => $viewed]);

	}
	
	public function ajaxVideo($pag = null) {
		
		$videos = File::where('path', 'LIKE', '%videos%') -> paginate($pag);
		
		$viewed = File::where('path', 'LIKE', '%videos%') -> where('viewed', '>', '5') -> limit(5) -> get();
		
		return view('pages.ajax.videos', ['videos' => $videos, 'viewed' => $viewed ]);
		
	}
	
	public function showVideo($id = null) {
		
		$video = File::where('id', '=', $id) -> get();
		
		$video[0] -> viewed = $video[0] -> viewed + 1;
		
		$video[0] -> save();
		
		
		
		$viewed = File::where('path', 'LIKE', '%videos%') -> where('viewed', '>', '5') -> orderBy('viewed') -> limit(5) -> get();
		
		$files = File::where('path', 'LIKE', '%videos%') -> get();
		
		$filesFiltered = $files -> filter(function($value, $key) use ($id) {
			return $value -> id == $id;
		});
	
		$videos = $filesFiltered -> flatten();
		//$videos = File::where('id', '=', $id) -> get();
		
		return view('pages.showVid', ['videos' => $videos, 'viewed' => $viewed, 'files' => $files, 'chck' => 0]);
		
	}
	
	public function ajaxLike($id = null) {
		
		//return "ID = " . $id;
		
		$like = File::where('id', '=', $id) -> get();
		
		$like[0] -> liked = $like[0] -> liked + 1;
		
		$like[0] -> save();
		
		echo "<button type='button' id='unlikeButt' class='btn btn-default' onclick='unLike();'>Unlike video</button>";
		echo "  ";
		echo "Video liked";
		
		
		
	}
	
	
	
	public function ajaxUnlike($id = null) {
	
		//return "ID = " . $id;
	
		$like = File::where('id', '=', $id) -> get();
	
		$like[0] -> liked = $like[0] -> liked - 1;
	
		$like[0] -> save();
	
		echo "<button type='button' id='likeButt' class='btn btn-default' onclick='like();'>Like video</button>";
		

	}
	
	
	
	public function ajaxDislike($id = null) {
	
		//return "ID = " . $id;
	
		$dislike = File::where('id', '=', $id) -> get();
	
		$dislike[0] -> disliked = $dislike[0] -> disliked + 1;
	
		$dislike[0] -> save();
	
		echo "<button type='button' id='unDislikeButt' class='btn btn-default' onclick='unDislike();'>Remove dislike</button>";
		echo "  ";
		echo "Video disliked";
	
	
	}
	
	
	public function ajaxUndislike($id = null) {
	
		//return "ID = " . $id;
	
		$dislike = File::where('id', '=', $id) -> get();
	
		$dislike[0] -> disliked = $dislike[0] -> disliked - 1;
	
		$dislike[0] -> save();
	
		echo "<button type='button' id='dislikeButt' class='btn btn-default' onclick='dislike();'>Dislike video</button>";
	
	}
	
	
	public function ajaxPref($value = null) {
		
		if($value == "viewed") {
			$title = "Most viewed";
			$viewed = File::where('path', 'LIKE', '%videos%') -> where('viewed', '>', '5') -> orderBy('viewed') -> limit(5) -> get();
			return view('pages.ajax.videoPref', ['viewed' => $viewed]);
			
		}
		
		if($value == "liked") {
			$title = "Most Liked";	
			$viewed = File::where('path', 'LIKE', '%videos%') -> where('liked', '>', '1') -> orderBy('liked') -> limit(5) -> get();
			
			return view('pages.ajax.videoPref', ['viewed' => $viewed]);
		}
		
		if($value == "disliked") {
			$title = "Most Disliked";
			$viewed = File::where('path', 'LIKE', '%videos%') -> where('disliked', '>', '1') -> orderBy('disliked')-> limit(5) -> get();
			return view('pages.ajax.videoPref', ['viewed' => $viewed]);
		}
		
		if($value == 'favorites') {
			
			if(Auth::check()) {
			
				$viewed = Auth::user() -> favorits;
			
				$filteredFavs = $viewed -> filter(function($value, $key) {
					return str_contains($value -> path, 'videos');
				});
			
					$return = $filteredFavs -> take(1);
			
					return view('pages.ajax.videoPref', ['viewed' => $return]);
			
			}
		}
		
	}
	
	
}



















