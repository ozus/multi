<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddingFileIdToCommentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comments', function(BluePrint $table) {
        	$table -> integer('file_id') -> unsigned();
        	$table -> foreign('file_id') -> references('id') -> on('files') -> onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('comments', function (Blueprint $table) {
    		$table->dropColumn('file_id');
    	});
    }
}
