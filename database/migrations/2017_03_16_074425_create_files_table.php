<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->increments('id');
            
            $table -> integer('usr_id') -> unsigned();
            $table -> foreign('usr_id') -> references('id') -> on('users') -> onDelete('cascade');
            
            $table -> string('path', 255) -> unique();
            $table -> string('name', 255) -> unique() -> comment('Name given to file by aplication');
            $table -> string('extention', 5);
            $table -> string('desc', 1000) -> nullable();
            
            $table -> string('user_name', 255) -> unigue() -> comment('Name given to file by user');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
