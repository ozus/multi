<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersFavoritsPivot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_favorits', function(BluePrint $table) {
        	
        	$table -> integer('usr_id') -> unsigned();
        	$table -> foreign('usr_id') -> references('id') -> on('users') -> onDelete('cascade');
        	
        	$table -> integer('fav_id') -> unsigned();
        	$table -> foreign('fav_id') -> references('id') -> on('favorits') -> onDelete('cascade');
        	
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_favorits');
    }
}
